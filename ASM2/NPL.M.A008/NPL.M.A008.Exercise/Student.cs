﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Exercise
{
    internal class Student
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public DateOnly EntryDate { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public decimal Mark { get; set; }
        public String Grade { get; set; }

        public Student(
       string name,
       string className,
       string gender,
       string relationship = "Single",
       DateOnly entryDate,
       int age,
       string address,
       decimal mark = 0,
       string grade = "F")
        {
            Name = name;
            Class = className;
            Gender = gender;
            Relationship = relationship;
            EntryDate = entryDate;
            Age = age;
            Address = address;
            Mark = mark;
            Grade = grade;
        }


    }
}
