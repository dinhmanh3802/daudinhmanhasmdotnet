﻿// See https://aka.ms/new-console-template for more information

using System.Text;
using System.Text.RegularExpressions;

static Boolean IsEmail(string email)
{
    string pattern = @"^[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$";
    string[] parts = email.Split('@');

    if (parts.Length != 2)
    {
        return false;
    }

    string localPart = parts[0];    
    string domainPart = parts[1];

    if (string.IsNullOrEmpty(localPart) || string.IsNullOrEmpty(domainPart))
    {
        return false;
    }

    if (localPart[0] == '.' || localPart[localPart.Length - 1] == '.')
    {
        return false;
    }

    if (localPart.Contains("..") || domainPart.Contains(".."))
    {
        return false;
    }

    if (domainPart[0] == '-' || domainPart[domainPart.Length - 1] == '-')
    {
        return false;
    }
    string[] domainParts = domainPart.Split('.');
    if (domainParts.Length > 1)
    {
        string topLevelDomain = domainParts[domainParts.Length - 1];
        if (Regex.IsMatch(topLevelDomain, @"^[0-9]+$"))
        {
            return false;
        }
    }
    Console.WriteLine("abc");
    return Regex.IsMatch(email, pattern);
}


Console.OutputEncoding = Encoding.UTF8;
Console.Write("Enter your email: ");
String email = Console.ReadLine();
Console.WriteLine(IsEmail(email));
Console.ReadLine(); 
