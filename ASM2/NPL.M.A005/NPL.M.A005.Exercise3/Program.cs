﻿// See https://aka.ms/new-console-template for more information

static void SortName(string[] inputArr)
{
    string[] names = inputArr;
    for (int i = 0; i < names.Length - 1; i++)
    {

        for (int j = 0; j < names.Length - i - 1; j++)
        {
            if (GetLastName(names[j]).CompareTo(GetLastName(names[j + 1])) > 0)
            {
                string tempName = names[j];
                names[j] = names[j+1];
                names[j+1] = tempName;
            }
            if (GetLastName(names[j]).CompareTo(GetLastName(names[j + 1])) == 0)
            {
                if (GetFirstName(names[j]).CompareTo(GetFirstName(names[j + 1])) > 0)
                {
                    string tempName = names[j];
                    names[j] = names[j + 1];
                    names[j + 1] = tempName;
                }
            }

        }
    }
    for (int i = 0; i < names.Length ; i++)
    {
        if (i < names.Length - 1)
        {
            Console.Write(names[i] + ", ");
        } else
        {
            Console.WriteLine(names[i]);
        }
    }
}
static string GetLastName(string fullName)
{
    string[] nameParts = fullName.Split(' ').Select(s => s.Trim()).ToArray();
    if (nameParts.Length > 1)
    {
        return nameParts.Last();
    }
    else
    {
        return fullName;
    }
}
static string GetFirstName(string fullName)
{
    string[] nameParts = fullName.Split(' ').Select(s => s.Trim()).ToArray();
    if (nameParts.Length > 1)
    {
        return nameParts.First();
    }
    else
    {
        return "";
    }
}

Console.WriteLine("Enter a list of full names: ");
string input = Console.ReadLine();
string[] fullNameArray = input.Split(",").Select(s => s.Trim()).ToArray();
SortName(fullNameArray);