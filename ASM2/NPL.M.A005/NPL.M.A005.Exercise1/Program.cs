﻿// See https://aka.ms/new-console-template for more information


using System.Text;

static void NormalizeName(string name)
{
    name = name.Trim();
    Console.Write(name[0].ToString().ToUpper());
    for (int i = 1; i < name.Length; i++)
    {
        if (name[i] == ' '  && name[i+1] != ' ')
        {
            Console.Write(" ");
        }
        else if(name[i-1] == ' ' && name[i] != ' ')
        {
            Console.Write(name[i].ToString().ToUpper());
        } else if (name[i] != ' ')
        {
            Console.Write(name[i].ToString().ToLower());
        }
    }
}

Console.OutputEncoding = Encoding.UTF8;
Console.Write("Enter a name: ");
String name = Console.ReadLine();
Console.Write("Name in right format: ");
NormalizeName(name);


