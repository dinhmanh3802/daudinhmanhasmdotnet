﻿// See https://aka.ms/new-console-template for more information

using NPL.M.A007.Exercise1;

Book book = new Book(123456789, "Harry Potter", "J.K.Rowling", "Kim Dong");

Console.WriteLine("|{0, -20} | {1, -20} | {2, -20} | {3, -20}|", "ISBN Number", "Book Name", "Author Name", "Publisher Name");
Console.WriteLine(book.GetBookInformation());

