﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise1
{
    internal class Book
    {
        public string BookName { get; set; }
        public int ISBN { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }

        public Book()
        {
        }

        public Book(int iSBN, string bookName, string authorName, string publisherName)
        {
            this.BookName = bookName;
            this.ISBN = iSBN;
            this.AuthorName = authorName;
            this.PublisherName = publisherName;
        }

        public String GetBookInformation()
        {
            string bookInformation = String.Format("|{0, -20} | {1, -20} | {2, -20} | {3, -20}|", this.ISBN ,this.BookName ,  this.AuthorName, this.PublisherName);
            return bookInformation;
        }
    }
}
