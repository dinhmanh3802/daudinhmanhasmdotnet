﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Ford: Car
    {
        public int year {  get; set; }
        public int manufacturerDiscount { get; set; }

        public override double GetSalePrice()
        {
            return regularPrice - manufacturerDiscount;
        }

        public Ford()
        {
        }

        public Ford(decimal speed, double regularPrice, string? color, int year, int manufacturerDiscount) : base(speed, regularPrice, color)
        {
            this.year = year;
            this.manufacturerDiscount = manufacturerDiscount;
        }
        public void GetPrice()
        {
            Console.WriteLine(color + " Ford Price: " + GetSalePrice() + "$");
        }
    }

}
