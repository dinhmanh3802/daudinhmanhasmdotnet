﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal abstract class Car
    {
        public decimal speed { get; set; }
        public double regularPrice { get; set; }
        public string? color { get; set; }

        public abstract double GetSalePrice();

        protected Car()
        {
        }

        protected Car(decimal speed, double regularPrice, string? color)
        {
            this.speed = speed;
            this.regularPrice = regularPrice;
            this.color = color;
        }
    }
}
