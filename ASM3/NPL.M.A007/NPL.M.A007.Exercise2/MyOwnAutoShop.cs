﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        Sedan sedan1 = new Sedan(120, 10000, "Viole", 120);
        Truck truck1 = new Truck(140, 20000, "Navy", 1000);
        Truck truck2 = new Truck(160, 25000, "Sliver", 2000);
        Ford ford1 = new Ford(125, 30000, "Gold", 2022, 999);
        Ford ford2 = new Ford(175,50000, "Black", 2020, 1999);

        public void getPriceOfAllCars()
        {
            sedan1.GetPrice();
            truck1.GetPrice();
            truck2.GetPrice();
            ford1.GetPrice();
            ford2.GetPrice();
        }

    }
}
