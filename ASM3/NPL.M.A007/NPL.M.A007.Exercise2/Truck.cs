﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck : Car
    {
        public int weight { get; set; }

        public override double GetSalePrice()
        {
            if (weight > 2000)
            {
                return regularPrice * 0.9;
            }
            else { return regularPrice * 0.8; }
        }

        public Truck()
        {
        }

        public Truck(decimal speed, double regularPrice, string? color, int weight) : base(speed, regularPrice, color)
        {
            this.weight = weight;
        }
        public void GetPrice()
        {
            Console.WriteLine(color + " Truck Price: " + GetSalePrice() + "$");
        }
    }
}
