﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Sedan: Car
    {
        public int length { get; set; }
        public override double GetSalePrice()
        {
            if (length > 20)
            {
                return regularPrice * 0.95;
            }
            else
            {
                return regularPrice * 0.9;
            }
        }

        public Sedan()
        {
        }

        public Sedan(decimal speed, double regularPrice, string? color, int lenght) : base(speed, regularPrice, color)
        {
            this.length = lenght;
        }

        public void GetPrice()
        {
            Console.WriteLine(color + " Sedan Price: " + GetSalePrice() + "$");
        } 
    }
}
