﻿// See https://aka.ms/new-console-template for more information
int n;
while (true)
{
    Console.Write("Nhap so phan tu cua mang: ");
    n = int.Parse(Console.ReadLine());
    if (n <= 0)
    {
        Console.WriteLine("So phan tu cua mang phai lon hon 0.");
    }
    else
    {
        break;
    }
}
int [] arr = new int[n];
for (int i = 0; i < n; i++)
{
    Console.Write("Phan tu thu " + (i+1) + " : ");
    arr[i] = int.Parse(Console.ReadLine());
}
int max = arr[0];
int min = arr[0];
for (int i = 1; i < n; i++)
{
    if (arr[i] > max)
    {
        max = arr[i];
    }
    if (arr[i] < min)
    {
        min = arr[i];
    }
}
Console.WriteLine("Max: " + max);
Console.WriteLine("Min: " + min);
Console.ReadLine();
