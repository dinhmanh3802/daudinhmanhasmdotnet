﻿// See https://aka.ms/new-console-template for more information
int n;
while (true)
{
    Console.Write("Nhap so phan tu cua mang: ");
    n = int.Parse(Console.ReadLine());
    if (n <= 0)
    {
        Console.WriteLine("So phan tu cua mang phai lon hon 0.");
    }
    else
    {
        break;
    }
}
int[] arr = new int[n];
for (int i = 0; i < n; i++)
{
    Console.Write("Phan tu thu " + (i + 1) + " : ");
    arr[i] = int.Parse(Console.ReadLine());
}
int gcd = 1;
for (int i = arr[0]; i > 0; i--)
{
    if (arr[0] % i == 0)
    {
        Boolean check = true;
        for (int j = 1; j < arr.Length; j++)
        {
            if (arr[j] % i != 0)
            {
                check = false;
            }
        }
        if (check)
        {
            gcd = i; break;
        }

    }
}
Console.WriteLine("Uoc chung lon nhat cua mang: " + gcd);

