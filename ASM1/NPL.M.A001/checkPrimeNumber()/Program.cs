﻿// See https://aka.ms/new-console-template for more information
static Boolean checkPrimeNumber(int n)
{
    int count = 0;
    for (int i = 2; i <= n / 2; i++)
    {
        if (n % i == 0)
        {
            count++;
        }
    }
    return count == 0;
}

Console.Write("Nhap so can kiem tra: ");
int n = int.Parse(Console.ReadLine());
if (checkPrimeNumber(n) && !(n == 1 || n == 0))0
{
    Console.WriteLine(n + " la SO NGUYEN TO.");
}
else
{
    Console.WriteLine(n + " KHONG PHAI la SO NGUYEN TO.");
}
Console.WriteLine();