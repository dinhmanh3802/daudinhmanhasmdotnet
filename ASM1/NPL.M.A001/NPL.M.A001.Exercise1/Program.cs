﻿// See https://aka.ms/new-console-template for more information
static void findX(double a, double b, double c)
{
    double delta = b * b - 4 * a * c;
    Console.Write("Phuong trinh " + a + "x^2+" + b + "x+" + c + "=0");
    if (a == 0 && b == 0 && c == 0)
    {
        Console.WriteLine(" co vo so nghiem!");
    }
    else if (a == 0 && b != 0)
    {
        Console.WriteLine(" co nghiem: x = " + -c / b);
    }
    else if ((a == 0 && b == 0) || delta < 0)
    {
        Console.WriteLine(" vo nghiem!");
    }
    else if (delta == 0)
    {
        double x = -b / 2 * a;
        Console.WriteLine(" co nghiem kep x1 = x2 = " + x);
    }
    else
    {
        double x1 = (-b - Math.Sqrt(delta)) / 2 * a;
        double x2 = (-b + Math.Sqrt(delta)) / 2 * a;
        Console.WriteLine(" co hai nghiem: x1 = " + x1 + "   |   x2 = " + x2);
    }
    Console.ReadLine();

}
Console.Write("Nhap a: ");
double a = double.Parse(Console.ReadLine());
Console.Write("Nhap b: ");
double b = double.Parse(Console.ReadLine());
Console.Write("Nhap c: ");
double c = double.Parse(Console.ReadLine());
findX(a, b, c);
