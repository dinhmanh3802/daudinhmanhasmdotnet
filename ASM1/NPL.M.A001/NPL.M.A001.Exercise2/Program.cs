﻿// See https://aka.ms/new-console-template for more information
static String convertToBinary(int dec)
{
    String bin = "";
    do
    {
        if(dec % 2 == 1 || dec % 2 == -1)
        {
            bin = "1" + bin;
        } else
        {
            bin = "0" + bin;
        }
        if(dec == -1)
        {
            bin = "-" + bin;
        }
        dec = dec / 2;
    } while (dec != 0);
    return bin;
}

Console.Write("Nhap so thap phan: ");
int dec = int.Parse(Console.ReadLine());
Console.WriteLine("So nhi phan tuong ung: " + convertToBinary(dec));
Console.ReadLine();